package expressionevaluator.suboperators;

import expressionevaluator.Operator;

public class IntegerDoubleSubOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left - (Double) right;
    }
}