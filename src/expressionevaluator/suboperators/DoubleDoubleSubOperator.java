package expressionevaluator.suboperators;

import expressionevaluator.Operator;

public class DoubleDoubleSubOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left - (Double) right;
    }
}