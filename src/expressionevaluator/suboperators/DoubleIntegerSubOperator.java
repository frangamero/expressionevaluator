package expressionevaluator.suboperators;

import expressionevaluator.Operator;

public class DoubleIntegerSubOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left - (Integer) right;
    }
}