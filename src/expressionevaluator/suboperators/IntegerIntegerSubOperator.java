package expressionevaluator.suboperators;

import expressionevaluator.Operator;

public class IntegerIntegerSubOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left - (Integer) right;
    }
}