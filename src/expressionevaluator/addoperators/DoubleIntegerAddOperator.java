package expressionevaluator.addoperators;

import expressionevaluator.Operator;

public class DoubleIntegerAddOperator implements Operator{ 
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left + (Integer) right;
    }
}