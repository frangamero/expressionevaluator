package expressionevaluator.addoperators;

import expressionevaluator.Operator;

public class DoubleDoubleAddOperator implements Operator{ 
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left + (Double) right;
    }
}
