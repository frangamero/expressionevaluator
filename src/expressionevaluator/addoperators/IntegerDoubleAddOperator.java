package expressionevaluator.addoperators;

import expressionevaluator.Operator;

public class IntegerDoubleAddOperator implements Operator{ 
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left + (Double) right;
    }
}