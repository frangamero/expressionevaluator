package expressionevaluator.addoperators;

import expressionevaluator.Operator;

public class IntegerIntegerAddOperator implements Operator{ 
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left + (Integer) right;
    }
}