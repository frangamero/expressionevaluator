package expressionevaluator.multoperators;

import expressionevaluator.Operator;

public class IntegerDoubleMultOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left * (Double) right;
    }
}