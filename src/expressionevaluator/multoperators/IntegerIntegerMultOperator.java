package expressionevaluator.multoperators;

import expressionevaluator.Operator;

public class IntegerIntegerMultOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left * (Integer) right;
    }
}