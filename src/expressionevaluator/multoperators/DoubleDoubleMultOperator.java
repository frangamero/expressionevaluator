package expressionevaluator.multoperators;

import expressionevaluator.Operator;

public class DoubleDoubleMultOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left * (Double) right;
    }
}
