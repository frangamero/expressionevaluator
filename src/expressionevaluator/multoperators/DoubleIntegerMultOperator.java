package expressionevaluator.multoperators;

import expressionevaluator.Operator;

public class DoubleIntegerMultOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left * (Integer) right;
    }
}