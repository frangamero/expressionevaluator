package expressionevaluator;

import expressionevaluator.addoperators.DoubleDoubleAddOperator;
import expressionevaluator.addoperators.DoubleIntegerAddOperator;
import expressionevaluator.addoperators.IntegerDoubleAddOperator;
import expressionevaluator.addoperators.IntegerIntegerAddOperator;
import expressionevaluator.divoperators.DoubleDoubleDivOperator;
import expressionevaluator.divoperators.DoubleIntegerDivOperator;
import expressionevaluator.divoperators.IntegerDoubleDivOperator;
import expressionevaluator.divoperators.IntegerIntegerDivOperator;
import expressionevaluator.multoperators.DoubleDoubleMultOperator;
import expressionevaluator.multoperators.DoubleIntegerMultOperator;
import expressionevaluator.multoperators.IntegerDoubleMultOperator;
import expressionevaluator.multoperators.IntegerIntegerMultOperator;
import expressionevaluator.suboperators.DoubleDoubleSubOperator;
import expressionevaluator.suboperators.DoubleIntegerSubOperator;
import expressionevaluator.suboperators.IntegerDoubleSubOperator;
import expressionevaluator.suboperators.IntegerIntegerSubOperator;
import java.util.HashMap;

public class BinaryOperationFactory {
    private static BinaryOperationFactory instance;
    private HashMap<String, Operator> operators;
    private BinaryOperationFactory() {
        operators = new HashMap<>();
        createOperators();    
    }
    
    public static BinaryOperationFactory getInstance(){
        if(instance==null) instance = new BinaryOperationFactory();
        return instance;
    }

    private void createOperators() {
        createAddOperators();
        createSubOperators();
        createMultOperators();
        createDivOperators();
    }
    
    public Operator getOperator (String signature){
        return operators.get(signature);
    }

    private void createAddOperators() {
        operators.put("DoubleDoubleAdd",new DoubleDoubleAddOperator()); 
        operators.put("DoubleIntegerAdd",new DoubleIntegerAddOperator());
        operators.put("IntegerDoubleAdd",new IntegerDoubleAddOperator());
        operators.put("IntegerIntegerAdd",new IntegerIntegerAddOperator());
    }

    private void createSubOperators() {
        operators.put("DoubleDoubleSub",new DoubleDoubleSubOperator());    
        operators.put("DoubleIntegerSub",new DoubleIntegerSubOperator());    
        operators.put("IntegerDoubleSub",new IntegerDoubleSubOperator());    
        operators.put("IntegerIntegerSub",new IntegerIntegerSubOperator());
    }
    
    private void createMultOperators() {
        operators.put("DoubleDoubleMult",new DoubleDoubleMultOperator());    
        operators.put("DoubleIntegerMult",new DoubleIntegerMultOperator());    
        operators.put("IntegerDoubleMult",new IntegerDoubleMultOperator());    
        operators.put("IntegerIntegerMult",new IntegerIntegerMultOperator());
    }
    
    private void createDivOperators() {
        operators.put("DoubleDoubleDiv",new DoubleDoubleDivOperator());    
        operators.put("DoubleIntegerDiv",new DoubleIntegerDivOperator());    
        operators.put("IntegerDoubleDiv",new IntegerDoubleDivOperator());    
        operators.put("IntegerIntegerDiv",new IntegerIntegerDivOperator()); 
    }
}