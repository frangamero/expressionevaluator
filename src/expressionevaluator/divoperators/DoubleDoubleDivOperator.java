package expressionevaluator.divoperators;

import expressionevaluator.Operator;

public class DoubleDoubleDivOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left / (Double) right;
    }
}
