package expressionevaluator.divoperators;

import expressionevaluator.Operator;

public class IntegerDoubleDivOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left / (Double) right;
    }
}