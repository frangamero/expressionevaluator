package expressionevaluator.divoperators;

import expressionevaluator.Operator;

public class DoubleIntegerDivOperator implements Operator{
    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left / (Integer) right;
    }
}