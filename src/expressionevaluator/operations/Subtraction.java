package expressionevaluator.operations;

import expressionevaluator.BinaryOperation;
import expressionevaluator.BinaryOperationFactory;
import expressionevaluator.Expression;

public class Subtraction extends BinaryOperation<Object> {    
    public Subtraction(Expression left, Expression right) {
        super(left, right);
    }
    
    @Override
    public Object evaluate() {
        return BinaryOperationFactory.getInstance().getOperator(getSignature()).evaluate(left.evaluate(), right.evaluate());
    }
    
    private String getSignature(){
        return left.evaluate().getClass().getSimpleName() + right.evaluate().getClass().getSimpleName()+"Sub";
    }
}
