package lexer;

public interface Validator {

    boolean validateInput(String text);
}
