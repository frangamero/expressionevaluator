package lexer;

import java.util.ArrayList;
import java.util.StringTokenizer;
import parser.Token;

public class Tokenizer {
    public Token[] tokenize(String text) {
        if(!new NumericValidator().validateInput(text)) return null;        
        ArrayList <Token> tokens = buildTokenList(extractSymbols(text), extractConstants(text));
        return convertToTokenArray(tokens);
    }

    private ArrayList<Token> buildTokenList(ArrayList<Token> symbols, ArrayList<Token> constants) {
        ArrayList<Token> tokens = new ArrayList<>();
        for (int i = 0; i < symbols.size(); i++) {
            tokens.add(constants.get(i));
            tokens.add(symbols.get(i));
        }
        tokens.add(constants.get(constants.size()-1));
        return tokens;
    }
    
    private ArrayList<Token> execute(String text, String separator, Function function){
        ArrayList<Token> tokenList = new ArrayList<>();
        StringTokenizer tokens = new StringTokenizer(text,separator);
        while(tokens.hasMoreTokens()){
            tokenList.add(function.apply(tokens.nextToken()));
        }
        return tokenList;
    }
    
    private ArrayList<Token> extractConstants(String text){
        return execute(text, "+-*/", new Function() {
            @Override
            public Token apply(String text) {
                return convertToConstant(text);
            }
        });
    }
    
    private ArrayList<Token> extractSymbols(String text){
        return execute(text, "0123456789.", new Function() {
            @Override
            public Token apply(String text) {
                OperatorDictionary operatorDictionay = new OperatorDictionary();
                return operatorDictionay.getSymbol(text);
            }
        });
    }

    private Token.Constant convertToConstant(String number) {
        if(number.indexOf(".") > -1){
            return new Token.Constant(Double.parseDouble(number));
        }
        return new Token.Constant(Integer.parseInt(number));
    }

    private Token[] convertToTokenArray(ArrayList<Token> tokens) {
        Token[] array = new Token[tokens.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = tokens.get(i);
        }
        return array;
    }
    
    private interface Function{
        public Token apply(String text);
    }
}
