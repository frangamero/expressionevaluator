package lexer;

public class NumericValidator implements Validator {
    
    private boolean lastCharacterSymbol;
    private boolean lastCharacterPoint;
    private boolean pointBetweenSymbols;

    public NumericValidator() {
        lastCharacterSymbol = true;
        lastCharacterPoint = false;
        pointBetweenSymbols = false;
    }

    @Override
    public boolean validateInput(String text) {
        for (int i = 0; i < text.length()-1; i++) 
            if (checkCharacter(text.charAt(i))) return false;

        return validateLastSymbol(text.charAt(text.length() - 1));
    }

    private boolean validateCharacter(char character) {
        if (!isOnDictionary(character)) return false;
        if (isSymbol(character) && (lastCharacterSymbol || lastCharacterPoint)) return false;
        return !(isPoint(character) && (lastCharacterPoint || lastCharacterSymbol || pointBetweenSymbols));
    }

    private void setFlags(char character) {
        lastCharacterSymbol = isSymbol(character);
        if (lastCharacterSymbol) pointBetweenSymbols = false;
        lastCharacterPoint = isPoint(character);
        if (lastCharacterPoint) pointBetweenSymbols = true;
    }

    private boolean isSymbol(char character) {
        return "+-*/".indexOf(character) > -1;
    }

    private boolean isNumber(char character) {
        return Character.isDigit(character);
    }

    private boolean isPoint(char character) {
        return character == '.';
    }

    private boolean isOnDictionary(char character) {
        return "0123456789+-*/.".indexOf(character) > -1;
    }

    private boolean validateLastSymbol(char character) {
        return isNumber(character);
    }

    private boolean checkCharacter(char character) {
        if (!validateCharacter(character)) return true;
        setFlags(character);
        return false;
    }
}