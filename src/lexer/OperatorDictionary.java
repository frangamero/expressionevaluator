package lexer;

import java.util.HashMap;
import parser.Token;

public class OperatorDictionary {
    private final HashMap<String, Token.Symbol> dictionay;
    
    public OperatorDictionary(){
        dictionay = new HashMap<>();
        dictionay.put("+", Token.Symbol.ADD);
        dictionay.put("-", Token.Symbol.SUB);
        dictionay.put("*", Token.Symbol.MUL);
        dictionay.put("/", Token.Symbol.DIV);
    }
    
    public Token.Symbol getSymbol(String symbol){
        return dictionay.get(symbol);
    }
}
