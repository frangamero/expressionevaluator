package parser;

import expressionevaluator.Expression;

public interface Parser{
    
    public Expression parse(Token[] tokens);
}
