package parser;

public abstract class Token {
    
    public static <Type> Constant constant(Type value){
        return new Constant(value);
    }
    
    public static class Symbol extends Token{
        private final String symbol;
        private final int priority; 
        
        public static final Symbol ADD = new Symbol("+", 0);
        public static final Symbol SUB = new Symbol("-", 0);
        public static final Symbol MUL = new Symbol("*", 1);
        public static final Symbol DIV = new Symbol("/", 1);
        
        private Symbol(String symbol, int priority) {
            this.symbol = symbol;
            this.priority = priority;
        }

        public String symbol() {
            return symbol;
        }
        
        public int comparePriority(Token.Symbol symbol){
            return priority - symbol.priority;
        }
    }
    
    public static class Constant<Type> extends Token{
        private final Type value;

        public Constant(Type constant) {
            this.value = constant;
        }

        public Type constant() {
            return value;
        }
    }
}
