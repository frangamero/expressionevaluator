package parser;

import expressionevaluator.Expression;
import expressionevaluator.operations.Addition;
import expressionevaluator.operations.Division;
import expressionevaluator.operations.Multiplication;
import expressionevaluator.operations.Subtraction;
import java.util.HashMap;

public class ExpressionFactory {
    private static ExpressionFactory instance;
    private HashMap<String, Builder> operators;
    
    public static ExpressionFactory getInstance(){
        if(instance == null)instance = new ExpressionFactory();
        return instance;
    }
    private ExpressionFactory(){
        operators = new HashMap<>();
        operators.put("+",new Builder() {
            @Override
            public Expression build(Expression left, Expression right) {
                return new Addition(left, right);
            }
        });
        
        operators.put("-",new Builder() {
            @Override
            public Expression build(Expression left, Expression right) {
                return new Subtraction(left, right);
            }
        });
        
        operators.put("*",new Builder() {
            @Override
            public Expression build(Expression left, Expression right) {
                return new Multiplication(left, right);
            }
        });
        
        operators.put("/",new Builder() {
            @Override
            public Expression build(Expression left, Expression right) {
                return new Division(left, right);
            }
        });
    }
    
    public Expression build(Token.Symbol symbol, Expression left, Expression right){
        return operators.get(symbol.symbol()).build(left, right);
    }
}

interface Builder{
    public Expression build(Expression left, Expression right);
}
