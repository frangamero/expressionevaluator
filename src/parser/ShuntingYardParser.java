package parser;

import expressionevaluator.Constant;
import expressionevaluator.Expression;
import java.util.Stack;

public class ShuntingYardParser implements Parser {

    private Stack<Expression> expressionStack;
    private Stack<Token.Symbol> symbolStack;

    public ShuntingYardParser() {
        this.symbolStack = new Stack<>();
        this.expressionStack = new Stack<>();
    }

    @Override
    public Expression parse(Token[] tokens) {
        for (Token token : tokens) {
            parse(token);
        }

        while (!symbolStack.empty()) {
            Token.Symbol symbol = symbolStack.pop();
            Expression right = expressionStack.pop();
            expressionStack.push(ExpressionFactory.getInstance().build(symbol, expressionStack.pop(), right));
        }
        return expressionStack.pop();
    }

    private void parse(Token newToken) {
        if (newToken instanceof Token.Symbol) {
            if (symbolStack.empty()) {
                symbolStack.push((Token.Symbol) newToken);
                return;
            }
            while (hasEqualOrLessPrecedence((Token.Symbol) newToken)) {
                Token.Symbol symbol = symbolStack.pop();
                Expression right = expressionStack.pop();
                expressionStack.push(ExpressionFactory.getInstance().build(symbol, expressionStack.pop(), right));
            }
            symbolStack.push((Token.Symbol) newToken);
        } else if (newToken instanceof Token.Constant) {
            expressionStack.push(new Constant(((Token.Constant) newToken).constant()));
        }
    }

    private boolean hasEqualOrLessPrecedence(Token.Symbol symbol) {
        if(symbolStack.empty())return false;
        return symbol.comparePriority(symbolStack.peek()) <= 0;
    }
}
