package expressionevaluator;

import expressionevaluator.operations.Subtraction;
import expressionevaluator.operations.Addition;
import expressionevaluator.operations.Multiplication;
import org.junit.Assert;
import org.junit.Test;

public class ExpressionTest {

    @Test
    public void constantTest(){
        Assert.assertEquals(5,new Constant(5).evaluate());
        Assert.assertEquals(2.3,new Constant(2.3).evaluate());
    }
    
    @Test
    public void binaryAddOperationTest(){
        Assert.assertEquals(5.2,new Addition(new Constant(3), new Constant(2.2)).evaluate());
        Assert.assertEquals(5.1,new Addition(new Constant(3.0), new Constant(2.1)).evaluate());
        Assert.assertEquals(5,new Addition(new Constant(3), new Constant(2)).evaluate());
        Assert.assertEquals(5.2,new Addition(new Constant(3.2), new Constant(2)).evaluate());
    }
    
    @Test
    public void binarySubOperationTest(){
        Assert.assertEquals(0.9, (double) new Subtraction(new Constant(3), new Constant(2.1)).evaluate(), 0.00001);
        Assert.assertEquals(0.9, (double) new Subtraction(new Constant(3.0), new Constant(2.1)).evaluate(), 0.00001);
        Assert.assertEquals(1, new Subtraction(new Constant(3), new Constant(2)).evaluate());
        Assert.assertEquals(1.2, (double) new Subtraction(new Constant(3.2), new Constant(2)).evaluate(), 0.00001);
    }
    
    @Test
    public void binaryMultOperationTest(){
        Assert.assertEquals(7.5, (double) new Multiplication(new Constant(3), new Constant(2.5)).evaluate(), 0.00001);
        Assert.assertEquals(6.3, (double) new Multiplication(new Constant(3.0), new Constant(2.1)).evaluate(), 0.00001);
        Assert.assertEquals(6, new Multiplication(new Constant(3), new Constant(2)).evaluate());
        Assert.assertEquals(6.4, (double) new Multiplication(new Constant(3.2), new Constant(2)).evaluate(), 0.00001);
    }   
}
