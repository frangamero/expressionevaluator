package parser;

import static org.junit.Assert.*;
import org.junit.Test;
import static parser.Token.*;

public class ParserTest {
    @Test
    public void expressionTestWithNoPriority(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(3),
            Token.Symbol.ADD,
            constant(1)
        };
        assertEquals(4, parser.parse(tokens).evaluate());
    }
    
    @Test
    public void expressionTestWithSamePriority(){ 
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(3),
            Token.Symbol.SUB,
            constant(1),
            Token.Symbol.ADD,
            constant(2),
            Token.Symbol.SUB,
            constant(4)
        };
        assertEquals(0, parser.parse(tokens).evaluate());
    }
    
    @Test
    public void expressionTestWithSamePriority2(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(3),
            Token.Symbol.ADD,
            constant(1),
            Token.Symbol.SUB,
            constant(2)
        };
        assertEquals(2, parser.parse(tokens).evaluate());
    }
    
    @Test
    public void expressionTestWithMorePriority(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(5),
            Token.Symbol.ADD,
            constant(3),
            Token.Symbol.MUL,
            constant(8)
        };
        assertEquals(29, parser.parse(tokens).evaluate());
    }
    
    @Test
    public void expressionTestWithMorePriority2(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(5),
            Token.Symbol.MUL,
            constant(3),
            Token.Symbol.ADD,
            constant(4)
        };
        assertEquals(19, parser.parse(tokens).evaluate());
    }
    @Test
    public void expressionTestWithSamePriority3(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(6),
            Token.Symbol.DIV,
            constant(3),
            Token.Symbol.MUL,
            constant(4)
        };
        assertEquals(8, parser.parse(tokens).evaluate());
    }
    @Test
    public void expressionTestWithMorePriority3(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(6),
            Token.Symbol.DIV,
            constant(3),
            Token.Symbol.SUB,
            constant(4)
        };
        assertEquals(-2, parser.parse(tokens).evaluate());
    }
    @Test
    public void expressionTestWithMorePriority4(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(6),
            Token.Symbol.SUB,
            constant(8),
            Token.Symbol.DIV,
            constant(4)
        };
        assertEquals(4, parser.parse(tokens).evaluate());
    }
    @Test
    public void expressionTestWithMorePriority5(){
        Parser parser = new ShuntingYardParser();
        Token[] tokens = {
            constant(30),
            Token.Symbol.SUB,
            constant(8),
            Token.Symbol.DIV,
            constant(4),
            Token.Symbol.SUB,
            constant(8)
        };
        assertEquals(20, parser.parse(tokens).evaluate());
    } 
}
