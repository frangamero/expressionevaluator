package lexer;

import static org.junit.Assert.*;
import org.junit.Test;

public class ValidatorTest {
    @Test
    public void oneNumberTest(){
        NumericValidator validator = new NumericValidator();
        assertTrue(validator.validateInput("2"));
    }
    @Test
    public void validOperationTest(){
        NumericValidator validator = new NumericValidator();
        assertTrue(validator.validateInput("2+4"));
    }
    @Test
    public void invalidOperationTest(){
        NumericValidator validator = new NumericValidator();
        assertFalse(validator.validateInput("2+*4"));
    }
    @Test
    public void invalidOperationTest2(){
        NumericValidator validator = new NumericValidator();
        assertFalse(validator.validateInput("2..0"));
    }
    @Test
    public void invalidOperationTest3(){
        NumericValidator validator = new NumericValidator();
        assertFalse(validator.validateInput("b2 + 3"));
    }
    @Test
    public void invalidOperationTest4(){
        NumericValidator validator = new NumericValidator();
        assertFalse(validator.validateInput("234.5+345-45."));
    }
}
