package lexer;

import org.junit.Test;
import static org.junit.Assert.*;
import parser.Parser;
import parser.ShuntingYardParser;

public class TokenizerTest {
    
    @Test
    public void tokenizerTest(){
        Parser parser = new ShuntingYardParser();
        Tokenizer tokenizer = new Tokenizer();
        assertEquals(40, parser.parse(tokenizer.tokenize("8+8*4")).evaluate());
    }
}
